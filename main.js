window.onload = function(){
  let pixels = document.getElementsByClassName('js--pixels');
  const sphere = document.getElementsByClassName('js--sphere');
  const red = document.getElementById('js--red');
  let brush = "black";
  let cursor = document.getElementById('js--cursor');
  const reset = document.getElementById('js--reset');
  const saveDrawing = document.getElementById('js--save');
  const load = document.getElementById('js--load');
  const drawing = document.getElementById('js--drawing');

  let saved;

  for(let i = 0; i < pixels.length; i++){
    pixels[i].onmouseenter = (event) =>{
      pixels[i].setAttribute('color',brush);
      console.log(i);

    }
  }
for(let i = 0; i < sphere.length; i++){
  sphere[i].onmouseenter = (event) =>{
    console.log("sphere");
    brush = sphere[i].getAttribute('color');
    cursor.setAttribute('color', brush);
  }
}

  reset.onmouseenter = (event) =>{
  console.log("reset");
  for(let i = 0; i < pixels.length; i++){
      pixels[i].setAttribute('color', 'black');
    }
  }

    saveDrawing.onmouseenter = (event) =>{
      console.log("save");
      saved = pixels;
      console.log(saved);
    }

    load.onmouseenter = (event) =>{
      pixels = saved;
      console.log("load");
      console.log(pixels);
    }

}
